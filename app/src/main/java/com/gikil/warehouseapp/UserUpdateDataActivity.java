package com.gikil.warehouseapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class UserUpdateDataActivity extends AppCompatActivity {

    private EditText txtUpdateUserName;
    private EditText txtUpdateUserLastName;
    private EditText txtUpdateUserEmail;
    private EditText txtUpdateUserPassword;
    private EditText txtUpdateUserPassword2;
    private TextView txtUpdateResponse;
    private Button btnUpdateUser;
    private ProgressBar updateProgressBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_update_data);

        initialize();

        btnUpdateUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    private void initialize() {

        txtUpdateUserName = findViewById(R.id.user_update_name);
        txtUpdateUserLastName = findViewById(R.id.user_update_surname);
        txtUpdateUserEmail = findViewById(R.id.user_update_email);
        txtUpdateUserPassword = findViewById(R.id.user_update_password);
        txtUpdateUserPassword2 = findViewById(R.id.user_update_password2);
        txtUpdateResponse = findViewById(R.id.txt_update_response);
        btnUpdateUser = findViewById(R.id.btn_update_update);
        updateProgressBar= findViewById(R.id.update_progressBar);
    }
}
