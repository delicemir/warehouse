package com.gikil.warehouseapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.gikil.warehouseapp.User.User;
import com.gikil.warehouseapp.User.UserListAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UserActivity extends AppCompatActivity {

    private RecyclerView userList;
    private UserListAdapter userListAdapter;
    private LinearLayout mainHolder;
    private TextView username;
    private Spinner userPrivileges;

    private List<User> users = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        userList = findViewById(R.id.user_recycler_list);
        mainHolder = findViewById(R.id.main_holder);
        username = findViewById(R.id.txt_username);
        userPrivileges = findViewById(R.id.cmb_user_privileges);

        mainHolder.setVisibility(View.GONE);

        setUserListAdapter();
    }


    private void setUserListAdapter() {

        users = getUserList();

        UserListAdapter.OnEmailClick emailClickListener = new UserListAdapter.OnEmailClick() {
            @Override
            public void onEmailClick(User user) {
                username.setText(user.getUsername());
                mainHolder.setVisibility(View.VISIBLE);
                Toast.makeText(UserActivity.this,user.getUserImg(),Toast.LENGTH_LONG).show();
            }
        };

        userListAdapter = new UserListAdapter(UserActivity.this, users);
        userListAdapter.setEmailClickListener(emailClickListener);
        userList.setLayoutManager(new LinearLayoutManager(UserActivity.this));
        userList.setAdapter(userListAdapter);

    }

    public List<User> getUserList() {

        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, AppConstants.API_USER_GET_ALL_USERS, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONArray userList = new JSONArray(response.getString("users"));
                            String msg = response.getString("message");
                            System.out.println(userList.toString());
                            System.out.println(response);
                            String firstName="";
                            String lastName="";
                            String email="";
                            String username="";
                            int level = 0;
                            String imgUrl="";
                            for (int i = 0; i < userList.length(); i++) {
                                JSONObject jsonPart = userList.getJSONObject(i);
                                firstName = jsonPart.getString("first_name");
                                lastName = jsonPart.getString("last_name");
                                username = jsonPart.getString("username");
                                email = jsonPart.getString("email");
                                level = jsonPart.getInt("level");
                                imgUrl = jsonPart.getString("img_url");
                                users.add(new User(firstName, lastName, email, username, "",level, imgUrl));
                            }
                            Toast.makeText(UserActivity.this,msg,Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception e) {
                            Toast.makeText(UserActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // display error
                Toast.makeText(UserActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {
                Collections.sort(users);  //sortira korisnike po imenu
                userListAdapter.notifyDataSetChanged();
            }
        });

        // Add the request to the RequestQueue.
        queue.add(jsObjRequest);

        return users;
    }

    public void closeMainHolder(View view) {
        mainHolder.setVisibility(View.GONE);
    }
}
