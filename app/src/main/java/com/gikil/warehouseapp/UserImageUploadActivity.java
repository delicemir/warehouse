package com.gikil.warehouseapp;

import android.content.CursorLoader;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.toolbox.Volley;
import com.gikil.warehouseapp.Helper.SQLiteHandler;
import com.gikil.warehouseapp.User.User;

import org.json.JSONObject;

import static com.gikil.warehouseapp.AppConstants.PICK_IMAGE_REQUEST;

public class UserImageUploadActivity extends AppCompatActivity {

    private ImageView imageView;
    private Button btnChoose;
    private Button btnUpload;
    private ProgressBar progressBar;

    private String filePath;
    private MediaPlayer mp;

    private SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_image_upload);

        mp = MediaPlayer.create(this, R.raw.button_click);

        imageView = findViewById(R.id.user_image);
        btnChoose = findViewById(R.id.btn_choose);
        btnUpload = findViewById(R.id.btn_upload);
        progressBar = findViewById(R.id.user_image_progressbar);

        db = new SQLiteHandler(UserImageUploadActivity.this);

        if (Build.VERSION.SDK_INT >= 23)
        {
            if (checkPermission())
            {
                // Code for above or equal 23 API Oriented Device
                // Your Permission granted already .Do next code
            }
            else {
                requestPermission(); // Code for permission
            }
        }
        else
        {
            // Code for Below 23 API Oriented Device
            // Do next code
        }
    }

    public void uploadImage(View view) {

        mp.start();
        progressBar.setVisibility(View.VISIBLE);
        RequestQueue queueUserImageUpload = Volley.newRequestQueue(UserImageUploadActivity.this);

        if (filePath != null) {

            uploadUserImage(queueUserImageUpload,filePath);
            //saveUserImageUrl();
        }
        else {
            Toast.makeText(getApplicationContext(), "Image not selected!", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.INVISIBLE);
        }

        queueUserImageUpload.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });

    }

    private void uploadUserImage(RequestQueue queueUserImageUpload, String filePath) {

        SimpleMultiPartRequest smr = new SimpleMultiPartRequest(Request.Method.POST, AppConstants.API_USER_IMAGE_UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        try {
                            JSONObject jObj = new JSONObject(response);
                            //{"user":null,"result":{"error":true,"message":"Not received any file!","imgPath":"error loadig image"},"img_update":"User image not updated!"}
                            String message = jObj.getJSONObject("result").getString("message");
                            String imgPath = jObj.getJSONObject("result").getString("imgPath");
                            Toast.makeText(UserImageUploadActivity.this, message + " imgpath: "+imgPath + " for user: "+db.getUserUsername(), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception e) {
                            // JSON error
                            Toast.makeText(UserImageUploadActivity.this, "Json error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(UserImageUploadActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        smr.addFile("image", filePath);
        // Add the request to the RequestQueue.

        smr.addStringParam(AppConstants.APICALL_USERNAME, db.getUserUsername());
        queueUserImageUpload.add(smr);
    }

    public void chooseImage(View view) {

        mp.start();
        imageBrowse();

    }

    private void imageBrowse() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if(requestCode == PICK_IMAGE_REQUEST){

                Uri picUri = data.getData();
                filePath = getPath(picUri);
                Log.d("picUri", picUri.toString());
                Log.d("filePath", filePath);

                imageView.setImageURI(picUri);
            }
        }
    }

    private String getPath(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(getApplicationContext(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PICK_IMAGE_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(UserImageUploadActivity.this,"Permission Granted, Now you can use local drive .",Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(UserImageUploadActivity.this,"Permission Denied, You cannot use local drive .",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(UserImageUploadActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(UserImageUploadActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(UserImageUploadActivity.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(UserImageUploadActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_IMAGE_REQUEST);
        }
    }

}
