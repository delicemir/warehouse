package com.gikil.warehouseapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.gikil.warehouseapp.Helper.SQLiteHandler;
import com.gikil.warehouseapp.Helper.SharedPreferencesManager;

public class UserProfileActivity extends AppCompatActivity {

    private Button btnUpdateUserImage;
    private Button btnUpdateUserData;
    private Button btnLogout;

    private SharedPreferencesManager sharedPreferencesManager;
    private SQLiteHandler db;

    private MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        mp = MediaPlayer.create(this, R.raw.button_click);

        btnUpdateUserImage=findViewById(R.id.btn_update_user_img);
        btnUpdateUserData = findViewById(R.id.btn_update_user_data);
        btnLogout = findViewById(R.id.btn_user_logout);

        db = new SQLiteHandler(UserProfileActivity.this);
        sharedPreferencesManager = new SharedPreferencesManager(UserProfileActivity.this);

        if(!sharedPreferencesManager.isLoggedIn()) {
            logoutUser();
        }

        btnUpdateUserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mp.start();
                Intent intent = new Intent(getApplicationContext(),UserImageUploadActivity.class);
                startActivity(intent);
            }
        });

        btnUpdateUserData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mp.start();
                Intent intent = new Intent(UserProfileActivity.this,UserUpdateDataActivity.class);
                startActivity(intent);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mp.start();
                logoutUser();
            }
        });

    }

    private void logoutUser() {
        sharedPreferencesManager.setLogin(false);
        db.deleteUsers();
        Intent intent = new Intent(UserProfileActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
