package com.gikil.warehouseapp.User;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gikil.warehouseapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder> {

    List<User> userList = new ArrayList<>();
    OnEmailClick onEmailClick;
    Context context;

    public void setEmailClickListener(OnEmailClick emailClickListener) {
        this.onEmailClick = emailClickListener;
    }

    public UserListAdapter(Context context, List<User> userList) {
        this.userList = userList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_in_list,parent,false);
        return new ViewHolder(view,onEmailClick);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        User userItem = userList.get(position);

        holder.firstName.setText(userItem.getFirst_name());
        holder.lastName.setText(userItem.getLast_name());
        holder.username.setText(userItem.getUsername());
        holder.email.setText(userItem.getEmail());

        if(userItem.getUserImg().isEmpty()){
            Picasso.with(context).load(R.drawable.person_placeholder).into(holder.userImage);
        }
        else {
            System.out.println(userItem.getUserImg());
            Picasso.with(context).load(userItem.getUserImg())
                    .placeholder(R.drawable.person_placeholder)
                    .into(holder.userImage);
        }

        holder.user=userItem;
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView firstName;
        public TextView lastName;
        public TextView username;
        public TextView email;
        public ImageView userImage;

        private User user;

        public ViewHolder(View itemView, final OnEmailClick click) {
            super(itemView);

            firstName = itemView.findViewById(R.id.txt_first_name);
            lastName = itemView.findViewById(R.id.txt_last_name);
            username = itemView.findViewById(R.id.txt_username);
            email = itemView.findViewById(R.id.txt_user_email);
            userImage = itemView.findViewById(R.id.img_user_avatar);

            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    click.onEmailClick(user);
                }
            };
            username.setOnClickListener(listener);
        }
    }

    public interface OnEmailClick {
        void onEmailClick(User user);
    }

}
