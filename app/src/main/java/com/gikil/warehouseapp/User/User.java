package com.gikil.warehouseapp.User;

import android.support.annotation.NonNull;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class User implements Comparable<User> {

    private String first_name;
    private String last_name;
    private String email;
    private String username;
    private String password;
    private int userLevel;   //privileges - administrator=1, operator=0
    private String userImg;

    private static Map<String, String> userParams = new HashMap<>();

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String first_name, String last_name, String email, String username, String password, int level, String userImg) {
        setFirst_name(first_name);
        setLast_name(last_name);
        setEmail(email);
        setUsername(username);
        setPassword(password);
        setUserLevel(level);
        setUserImg(userImg);
    }

    public User(String first_name, String last_name, String email, String username, String password, int level) {
        setFirst_name(first_name);
        setLast_name(last_name);
        setEmail(email);
        setUsername(username);
        setPassword(password);
        setUserLevel(level);
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(int userLevel) {
        this.userLevel = userLevel;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public Map<String, String> getUserParams() {
        return userParams;
    }

    public void setUserParams() {

        userParams.put("first_name", getFirst_name());
        userParams.put("last_name", getLast_name());
        userParams.put("email", getEmail());
        userParams.put("username", getUsername());
        userParams.put("password", getPassword());

    }

    public void setUserLoginParams() {
        userParams.put("username", getUsername());
    }

    @Override
    public String toString() {
        return "User{" +
                "first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", userLevel=" + userLevel +
                '}';
    }

    // comparator for sorting users by name
    public static Comparator<User> UserByName = new Comparator<User>() {
        @Override
        public int compare(User user, User t1) {
            return user.getFirst_name().toUpperCase().compareTo(t1.getFirst_name().toUpperCase());
        }
    };

    // comparator for sorting users by last name
    public static Comparator<User> UserByLastname = new Comparator<User>() {
        @Override
        public int compare(User user, User t1) {
            return user.getLast_name().toUpperCase().compareTo(t1.getLast_name().toUpperCase());
        }
    };

    // comparator for sorting users by username
    public static Comparator<User> UserByUsername = new Comparator<User>() {
        @Override
        public int compare(User user, User t1) {
            return user.getUsername().toUpperCase().compareTo(t1.getUsername().toUpperCase());
        }
    };

    @Override
    public int compareTo(@NonNull User user) {
        return getFirst_name().toUpperCase().compareTo(user.getFirst_name().toUpperCase());
    }
}
