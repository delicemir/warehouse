package com.gikil.warehouseapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gikil.warehouseapp.Helper.SharedPreferencesManager;
import com.gikil.warehouseapp.Password.Password;
import com.gikil.warehouseapp.User.User;

import org.json.JSONObject;

import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    private EditText userFirstName;
    private EditText userLastName;
    private EditText userEmail;
    private EditText userUsername;
    private EditText userPassword;
    private EditText userPassword2;
    private ProgressBar progressBar;
    private Button btnRegister;
    private Button btnLogin;

    private SharedPreferencesManager sharedPreferencesManager;
    private MediaPlayer mp;

    private User user;

    private String errorMsg="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initialize();

        if(sharedPreferencesManager.isLoggedIn()) {
            startMainActivity();
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mp.start();
                Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mp.start();
                progressBar.setVisibility(View.VISIBLE);
                if(validateUserData()) {
                    registerUser();
                    startMainActivity();
                }
                else {
                    Toast.makeText(RegisterActivity.this, errorMsg, Toast.LENGTH_LONG).show();
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    private void startMainActivity() {
        Intent intent = new Intent(RegisterActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void registerUser() {

        RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstants.API_USER_REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the response string.
                        try {
                            //{"error":true,"message":"Parameters first_name, last_name, email, username, password missing"}
                            JSONObject result = new JSONObject(response);

                            boolean error = result.getBoolean("error");
                            if(!error){
                                String user = result.getString("user");
                                Toast.makeText(RegisterActivity.this,
                                        result.getString("message")+" [ User: "+user+" ]",
                                        Toast.LENGTH_LONG).show();
                            }
                            else {
                                String errorMsg = result.getString("message");
                                Toast.makeText(RegisterActivity.this, errorMsg, Toast.LENGTH_LONG).show();
                            }
                        }
                        catch (Exception e) {
                            Toast.makeText(RegisterActivity.this, "Error: "+e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(RegisterActivity.this, "Wrong API web link! Error: "+error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {
            //adding parameters to the request
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return user.getUserParams();
            }
        };

        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {

                progressBar.setVisibility(View.INVISIBLE);
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    // validate and initialize parameters for user POST
    private boolean validateUserData() {

        if(!userFirstName.getText().toString().isEmpty() && !userLastName.getText().toString().isEmpty()
                && !userEmail.getText().toString().isEmpty()
                && !userUsername.getText().toString().isEmpty() && !userPassword.getText().toString().isEmpty()
                && !userPassword.getText().toString().isEmpty() && !userPassword2.getText().toString().isEmpty()
                && userPassword.getText().toString().equals(userPassword2.getText().toString()))
        {

            user = new User(userFirstName.getText().toString(),userLastName.getText().toString(),userEmail.getText().toString(),
                    userUsername.getText().toString(),Password.hashPassword(userPassword.getText().toString()),0);

            user.setUserParams();  //set parameters for POST

            clearFields();

            return true;
        }
        else if(!userPassword.getText().toString().equals(userPassword2.getText().toString())) {

            errorMsg="Passwords must be equal!";
            return false;
        }

        errorMsg="All fields must be filled!";
        return false;
    }

    private void clearFields() {

        userFirstName.setText("");
        userLastName.setText("");
        userEmail.setText("");
        userUsername.setText("");
        userPassword.setText("");
        userPassword2.setText("");

    }

    private void initialize() {

        userFirstName = findViewById(R.id.user_first_name);
        userLastName = findViewById(R.id.user_last_name);
        userEmail = findViewById(R.id.user_email);
        userUsername = findViewById(R.id.user_username);
        userPassword = findViewById(R.id.user_password);
        userPassword2 = findViewById(R.id.user_password2);
        progressBar = findViewById(R.id.progressBar);
        btnRegister =  findViewById(R.id.btn_register_user);
        btnLogin = findViewById(R.id.btn_login);
        progressBar.setVisibility(View.INVISIBLE);

        sharedPreferencesManager = new SharedPreferencesManager(RegisterActivity.this);

        mp = MediaPlayer.create(this, R.raw.button_click);
    }
}
