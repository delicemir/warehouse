package com.gikil.warehouseapp;

/**
 * Created by emir on 11/5/17.
 */

public class AppConstants {

    //SHARED_PREFERENCES
    public static final String SHARED_PREFERENCES = "WarehouseApp";
    public static final String SHARED_USERNAME = "username";

    // All Static variables
    // Database Version
    public static final int DATABASE_VERSION = 1;

    // Database Name
    public static final String DATABASE_NAME = "android_api";

    // Login table name
    public static final String TABLE_USER = "user";

    // Sqlite constants
    // Login Table Columns names
    public static final String KEY_ID = "id";
    public static final String KEY_UID = "unique_id";
    public static final String KEY_FIRST_NAME = "first_name";
    public static final String KEY_LAST_NAME = "last_name";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_LEVEL = "level";
    public static final String KEY_REGISTERED_AT = "registered_at";
    public static final String KEY_UPDATED_AT = "updated_at";

    // Shared pref mode
    public static final int PRIVATE_MODE = 0;

    // APICALL username
    public static final String APICALL_USERNAME = "username";

    //localhost for testing
    //public static final String API_USER_LOGIN_URL = "http://10.0.2.2/warehouse/ApiUser.php?apicall=login";

    // webhosting
    public static final String API_USER_LOGIN_URL = "http://erazmotest.com/ApiUser.php?apicall=login";

    // localhost for testing
    //public static final String API_USER_REGISTER_URL = "http://10.0.2.2/warehouse/ApiUser.php?apicall=create_user";

    // webhosting
    public static final String API_USER_REGISTER_URL = "http://erazmotest.com/ApiUser.php?apicall=create_user";


    //localhost for testing
    //public static final String API_USER_IMAGE_UPLOAD_URL = "http://10.0.2.2/warehouse/ApiUser.php?apicall=user_image_upload";

    //webhosting
    public static final String API_USER_IMAGE_UPLOAD_URL = "http://erazmotest.com/ApiUser.php?apicall=user_image_upload";


    // for user image upload
    public static final int PICK_IMAGE_REQUEST = 1;

    // localhost for testing
    //public static final String API_USER_GET_ALL_USERS = "http://10.0.2.2/warehouse/ApiUser.php?apicall=get_users";

    // webhosting
    public static final String API_USER_GET_ALL_USERS = "http://erazmotest.com/ApiUser.php?apicall=get_users";

    /* JSON response for login

    * {"error":false,
    * "user":{"unique_id":"5a00a826b62d67.99229918","first_name":"Emir","last_name":"Delic","email":"emir@gmail.com","username":"emirko","password":"$2y$10$yrSQhAGpisW86aJgVlARwOjUJa3F7PivwJDfosaj\/Zj\/mp5Q0mtw6","level":0,"registered_at":"2017-11-06 19:21:26","updated_at":"0000-00-00 00:00:00"},
    * "msg_login_dt":{"status":"Saved user login date-time!"},"message":"User logged in!"}
    *
    * */


}
