/**
 * Author: Ravi Tamada
 * URL: www.androidhive.info
 * twitter: http://twitter.com/ravitamada
 * */
package com.gikil.warehouseapp.Helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.gikil.warehouseapp.AppConstants;

import java.util.HashMap;

public class SQLiteHandler extends SQLiteOpenHelper {

	private static final String TAG = SQLiteHandler.class.getSimpleName();

	public SQLiteHandler(Context context) {
		super(context, AppConstants.DATABASE_NAME, null, AppConstants.DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_LOGIN_TABLE = "CREATE TABLE " + AppConstants.TABLE_USER + "("
				+ AppConstants.KEY_ID + " INTEGER PRIMARY KEY," + AppConstants.KEY_UID + " TEXT,"
				+ AppConstants.KEY_FIRST_NAME + " TEXT," + AppConstants.KEY_LAST_NAME + " TEXT,"
				+ AppConstants.KEY_EMAIL + " TEXT," + AppConstants.KEY_USERNAME + " TEXT UNIQUE,"
				+ AppConstants.KEY_PASSWORD + " TEXT," + AppConstants.KEY_LEVEL + " INTEGER,"
				+ AppConstants.KEY_REGISTERED_AT + " TEXT," + AppConstants.KEY_UPDATED_AT + " TEXT" + ")";

		db.execSQL(CREATE_LOGIN_TABLE);

		Log.d(TAG, "Database tables created");
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + AppConstants.TABLE_USER);

		// Create tables again
		onCreate(db);
	}

	public void addUser(String uid ,String firstName, String lastName, String email, String username,
						String password, int level, String registeredAt, String updatedAt) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(AppConstants.KEY_UID, uid);
		values.put(AppConstants.KEY_FIRST_NAME, firstName);
		values.put(AppConstants.KEY_LAST_NAME, lastName);
		values.put(AppConstants.KEY_EMAIL, email);
		values.put(AppConstants.KEY_USERNAME, username);
		values.put(AppConstants.KEY_PASSWORD, password);
		values.put(AppConstants.KEY_LEVEL, level);
		values.put(AppConstants.KEY_REGISTERED_AT, registeredAt);
		values.put(AppConstants.KEY_UPDATED_AT, updatedAt);

		// Inserting Row
		long id = db.insert(AppConstants.TABLE_USER, null, values);
		db.close(); // Closing database connection

		Log.d(TAG, "New user inserted into sqlite: " + id);
	}
/*
	public HashMap<String, String> getUserDetails() {
		HashMap<String, String> user = new HashMap<String, String>();
		String selectQuery = "SELECT  * FROM " + AppConstants.TABLE_USER;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		// Move to first row
		cursor.moveToFirst();
		if (cursor.getCount() > 0) {
			user.put("name", cursor.getString(1));
			user.put("email", cursor.getString(2));
			user.put("uid", cursor.getString(3));
			user.put("created_at", cursor.getString(4));
		}
		cursor.close();
		db.close();
		// return user
		Log.d(TAG, "Fetching user from Sqlite: " + user.toString());

		return user;
	}
*/
	public String getUserUsername() {

		String selectQuery = "SELECT * FROM " + AppConstants.TABLE_USER;
		String username = "";
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		// Move to first row
		cursor.moveToFirst();
		if (cursor.getCount() > 0) {
			username = cursor.getString(5);
		}
		cursor.close();
		db.close();

		return username;
	}

	public void deleteUsers() {
		SQLiteDatabase db = this.getWritableDatabase();
		// Delete All Rows
		db.delete(AppConstants.TABLE_USER, null, null);
		db.close();

		Log.d(TAG, "Deleted all user info from sqlite");
	}

}
