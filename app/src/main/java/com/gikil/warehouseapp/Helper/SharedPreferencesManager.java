package com.gikil.warehouseapp.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.gikil.warehouseapp.AppConstants;

public class SharedPreferencesManager {
	// LogCat tag
	private static String TAG = SharedPreferencesManager.class.getSimpleName();

	// Shared Preferences
	SharedPreferences pref;

	Editor editor;
	Context _context;

	public static final String KEY_IS_LOGGED_IN = "isLoggedIn";

	public SharedPreferencesManager(Context context) {
		this._context = context;
		pref = _context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, AppConstants.PRIVATE_MODE);
		editor = pref.edit();
	}

	public void setLogin(boolean isLoggedIn) {

		editor.putBoolean(KEY_IS_LOGGED_IN, isLoggedIn);

		// commit changes
		editor.commit();

		Log.d(TAG, "User login session modified!");
	}
	
	public boolean isLoggedIn(){
		return pref.getBoolean(KEY_IS_LOGGED_IN, false);
	}
}
