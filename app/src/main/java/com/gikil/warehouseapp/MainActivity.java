package com.gikil.warehouseapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.gikil.warehouseapp.Helper.SQLiteHandler;
import com.gikil.warehouseapp.Helper.SharedPreferencesManager;

public class MainActivity extends AppCompatActivity {

    private Button btnAdd;
    private Button btnPreviewUsers;
    private Button btnMyProfile;
    private Button btnLogout;

    private SharedPreferencesManager sharedPreferencesManager;
    private SQLiteHandler db;

    private MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();

        if(!sharedPreferencesManager.isLoggedIn()) {
            logoutUser();
        }

        btnPreviewUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mp.start();
                Intent intent = new Intent(MainActivity.this,UserActivity.class);
                startActivity(intent);
            }
        });

        btnMyProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mp.start();
                Intent intent = new Intent(MainActivity.this,UserProfileActivity.class);
                startActivity(intent);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mp.start();
                logoutUser();
            }
        });

    }

    private void logoutUser() {
        sharedPreferencesManager.setLogin(false);
        db.deleteUsers();
        Intent intent = new Intent(MainActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void initialize() {

        btnPreviewUsers = findViewById(R.id.btn_view_users);
        btnAdd = findViewById(R.id.btn_add_user);
        btnMyProfile = findViewById(R.id.btn_user_profile);
        btnLogout = findViewById(R.id.btn_user_logout);

        db = new SQLiteHandler(MainActivity.this);
        sharedPreferencesManager = new SharedPreferencesManager(MainActivity.this);

        mp = MediaPlayer.create(this, R.raw.button_click);
    }
}
