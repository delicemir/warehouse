package com.gikil.warehouseapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;

import com.gikil.warehouseapp.Helper.SQLiteHandler;
import com.gikil.warehouseapp.Helper.SharedPreferencesManager;
import com.gikil.warehouseapp.User.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private EditText userName;
    private EditText userPassword;
    private Button btnLoginUser;
    private ProgressBar progressBarLogin;
    private TextView errorPassword;
    private Button btnRegister;

    private MediaPlayer mp;

    private SQLiteHandler db;
    private SharedPreferencesManager sharedPreferencesManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initialize();

        if(sharedPreferencesManager.isLoggedIn()) {
            startMainActivity();
        }

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mp.start();
                Intent intent = new Intent(getApplicationContext(),RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnLoginUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressBarLogin.setVisibility(View.VISIBLE);
                mp.start();
                String inputUserName = userName.getText().toString().trim();
                String inputUserPass = userPassword.getText().toString().trim();

                if(!inputUserName.isEmpty() && !inputUserPass.isEmpty()) {
                    checkUserLogin(inputUserName,inputUserPass);
                }
                else {
                    progressBarLogin.setVisibility(View.INVISIBLE);
                    Toast.makeText(LoginActivity.this,"Please enter your username and password!",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void startMainActivity() {
        Intent intent = new Intent(LoginActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void checkUserLogin(final String inputUserName, final String inputUserPass) {

        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConstants.API_USER_LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject result = new JSONObject(response);
                            //{"error":true,"message":"Parameters username, password missing"}
                            boolean error = result.getBoolean("error");
                            if(!error){
                                String msg = result.getString("message")+ " "
                                        + result.getJSONObject("msg_login_dt").getString("status");
                                sharedPreferencesManager.setLogin(true);
                                saveUserToSqliteDb(result);
                                Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_LONG).show();
                                startMainActivity();
                            }
                            else {
                                String errorMsg = result.getString("error_msg");
                                Toast.makeText(LoginActivity.this, errorMsg, Toast.LENGTH_LONG).show();
                            }
                        }
                        catch (JSONException e) {
                            Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("username",inputUserName);
                params.put("password",inputUserPass);

                return params;
            }
        };

        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {
                progressBarLogin.setVisibility(View.INVISIBLE);
            }
        });

        queue.add(stringRequest);
    }

    private void saveUserToSqliteDb(JSONObject result) {

        try {
            JSONObject user = result.getJSONObject("user");
            String uniqueId = user.getString(AppConstants.KEY_UID);
            String firstName = user.getString(AppConstants.KEY_FIRST_NAME);
            String lastName = user.getString(AppConstants.KEY_LAST_NAME);
            String email = user.getString(AppConstants.KEY_EMAIL);
            String username = user.getString(AppConstants.KEY_USERNAME);
            String password = user.getString(AppConstants.KEY_PASSWORD);
            int level = user.getInt(AppConstants.KEY_LEVEL);
            String registeredAt = user.getString(AppConstants.KEY_REGISTERED_AT);
            String updatedAt = user.getString(AppConstants.KEY_UPDATED_AT);

            db.addUser(uniqueId,firstName,lastName,email,username,password,level,registeredAt,updatedAt);
        }
        catch (JSONException e) {
            Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void initialize() {

        userName = findViewById(R.id.login_user_name);
        userPassword = findViewById(R.id.login_user_pass);
        btnLoginUser = findViewById(R.id.btn_login_user);
        progressBarLogin = findViewById(R.id.progressBarLogin);
        errorPassword = findViewById(R.id.txt_error_password);
        btnRegister = findViewById(R.id.btn_register);

        errorPassword.setText("");
        progressBarLogin.setVisibility(View.INVISIBLE);

        db = new SQLiteHandler(LoginActivity.this);
        sharedPreferencesManager = new SharedPreferencesManager(LoginActivity.this);

        mp = MediaPlayer.create(this, R.raw.button_click);
    }

}
